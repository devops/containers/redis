SHELL = /bin/bash

base_image = gitlab-registry.oit.duke.edu/devops/containers/debian-buster:v1

redis_major ?= 5

ifeq ($(redis_major), 4)
	redis_version = 4.0.14
	redis_download_sha256 = 1e1e18420a86cfb285933123b04a82e1ebda20bfb0a289472745a087587e93a7
else ifeq ($(redis_major), 5)
	redis_version = 5.0.10
	redis_download_sha256 = e30a5e7d1593a715cdda2a82deb90190816d06c9d1dc1ef5b36874878c683382
else ifeq ($(redis_major), 6)
	redis_version = 6.0.10
	redis_download_sha256 = 79bbb894f9dceb33ca699ee3ca4a4e1228be7fb5547aeb2f99d921e86c1285bd
endif

build_tag ?= redis-$(redis_major)

.PHONY : build
build:
	docker build --pull -t $(build_tag) \
		--build-arg base_image=$(base_image) \
		--build-arg redis_version=$(redis_version) \
		--build-arg redis_download_sha256=$(redis_download_sha256) \
		- \
		< ./Dockerfile

.PHONY : test
test:
	build_tag=$(build_tag) ./test.sh
