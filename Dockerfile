ARG base_image
FROM ${base_image}

ARG redis_version
ARG redis_download_sha256

ENV REDIS_DOWNLOAD_URL="http://download.redis.io/releases/redis-${redis_version}.tar.gz"

RUN set -eux; \
	apt-get -y update; \
	apt-get -y upgrade; \
	apt-get -y install gcc libc6-dev make; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/*

SHELL ["/bin/bash", "-c"]

RUN set -eux; \
	curl -sL -o redis.tar.gz $REDIS_DOWNLOAD_URL; \
	echo "${redis_download_sha256}  redis.tar.gz" | sha256sum -c - ; \
	mkdir -p /usr/src/redis; \
	tar zxf redis.tar.gz -C /usr/src/redis --strip-components=1; \
	rm redis.tar.gz; \
	cd /usr/src/redis; \
	make; \
	make install; \
	sed -E \
	-e 's,^(bind 127\.0\.0\.1),#\1,' \
	-e 's,^(protected-mode) yes,\1 no,' \
	redis.conf > /etc/redis.conf \
	; \
	rm -r /usr/src/redis; \
	# WARNING you have Transparent Huge Pages (THP) support enabled in your kernel.
	# This will create latency and memory usage issues with Redis.
	# To fix this issue run the command ...
	# (can't write to read-only file system)
	# echo 'never' > /sys/kernel/mm/transparent_hugepage/enabled; \
	redis-cli --version; \
	redis-server --version

RUN set -eux; \
	# This follows official Redis Docker image build ...
	groupadd -r -g 999 redis; \
	useradd -r -g redis -u 999 redis; \
	mkdir -p /data; \
	chown -R redis:redis /data

VOLUME /data

# Redis will write data here by default
WORKDIR /data

HEALTHCHECK CMD redis-cli PING

EXPOSE 6379

USER redis

CMD ["redis-server", "/etc/redis.conf"]
